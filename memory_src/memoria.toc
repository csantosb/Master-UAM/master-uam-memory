\select@language {spanish}
\contentsline {part}{I\hspace {1em}Presentaci\'on}{3}{part.1}
\contentsline {section}{\numberline {1}Introducci\'on}{3}{section.1}
\contentsline {section}{\numberline {2}Componentes y etapas del desarrollo}{4}{section.2}
\contentsline {section}{\numberline {3}Modulos utilizados}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}FMC112}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Shuttle LX1}{5}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Librerias FrontPanel (Hardware)}{6}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Librerias FrontPanel (Software)}{6}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Cubox}{7}{subsection.3.3}
\contentsline {section}{\numberline {4}Metodolog\IeC {\'\i }a de trabajo}{9}{section.4}
\contentsline {part}{II\hspace {1em}Desarrollo}{11}{part.2}
\contentsline {section}{\numberline {1}Software}{11}{section.1}
\contentsline {subsection}{\numberline {1.1}Librer\IeC {\'\i }a de nivel intermedio}{11}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Nivel de aplicaci\'on}{13}{subsection.1.2}
\contentsline {section}{\numberline {2}Firmware}{15}{section.2}
\contentsline {subsection}{\numberline {2.1}Comunicaci\'on de bajo nivel I}{15}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Comunicaci\'on de bajo nivel II}{19}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Tests funcionales}{20}{subsection.2.3}
\contentsline {section}{\numberline {3}Comunicaci\'on a trav\'es de un procesador}{22}{section.3}
\contentsline {subsection}{\numberline {3.1}Creaci\'on de la plataforma}{23}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Rutinas software sobre microBlaze}{24}{subsection.3.2}
\contentsline {section}{\numberline {4}FMC112}{28}{section.4}
\contentsline {subsection}{\numberline {4.1}Descripci\'on del hardware}{28}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Control de la tarjeta}{29}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Procesado de datos}{32}{subsection.4.3}
\contentsline {part}{III\hspace {1em}Resultados}{35}{part.3}
\contentsline {section}{\numberline {1}Montaje Final}{35}{section.1}
\contentsline {section}{\numberline {2}Conclusi\'on y perspectivas}{37}{section.2}
\contentsline {section}{\numberline {A}Ap\'endice A}{40}{appendix.Alph1}
