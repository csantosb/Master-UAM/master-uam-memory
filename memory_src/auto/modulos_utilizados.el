(TeX-add-style-hook
 "modulos_utilizados"
 (lambda ()
   (LaTeX-add-labels
    "sec:fmc112"
    "sec:shuttle-lx1"
    "fig:shuttle-lx1"
    "sec:librerias-frontpanel-hardware"
    "fig:frontpanel-endpoints"
    "sec:librerias-frontpanel-software"
    "sec:cubox"
    "fig:cubox")))

