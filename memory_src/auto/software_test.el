(TeX-add-style-hook
 "software_test"
 (lambda ()
   (LaTeX-add-labels
    "subfig-1-1:fp-top"
    "subfig-1-2:fp-core"
    "fig:ub-soft"
    "subfig-1-1:interrupt"
    "fig:usb-soft-2")))

