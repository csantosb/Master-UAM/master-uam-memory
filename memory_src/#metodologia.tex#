
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "memoria"
%%% End:

  En paralelo al desarrollo realizado, y como complemento  de éste, la ejecución de este proyecto ha
producido un  efecto beneficioso anexo en  cuanto a la metodología  de trabajo que se  ha seguido de
cara a su ejecución  en los plazos requeridos. Así, este proyecto ha  pretendido ser planteado desde
una  óptica novedosa,  lo que  ha conducido  a una  metodología de  trabajo también  original.  Tras
proceder a  un análisis  inicial de  los requerimientos  necesarios para  gestionar el  proyecto, ha
podido ponerse  en evidencia  que en el  curso de este  desarrollo debería  de hacerse frente  a una
variedad importante de metodologías, lenguajes y  herramientas, lo que conlleva una carga importante
en todo lo relativo a su organización.

  Así, por  una parte se ha  procedido a realizar una  primera etapa de documentación  y prospección
bibliográfica, que ha llevado a dar forma a  este proyecto. Posteriormente, se ha debido de proceder
a concebir  código (tanto  embebido como  software) en varios  lenguajes de  programación distintos:
vhdl, c, python.  Por otra parte, ha sido necesario redactar una memoria de proyecto con sus fuentes
en \LaTeX, automatizando  su exportación hacia pdf,  html, etc.  Ha sido  necesario además documentar
todo lo  anterior, organizando  y gestionando las  diversas etapas seguidas.   Durante el  curso del
proyecto, se  ha pretendido  además gestionar  y organizar las  anotaciones, comentarios  y ficheros
adjuntos a cada etapa, integrándolos bien a la documentación, bien a la propia memoria del proyecto.

  Con la finalidad de organizar todo lo anterior, se ha optado por una metodología de trabajo lo mas
ergonómica y sencilla posible, realizando un ejercicio  interesante de gestión global de proyecto de
desarrollo  en paralelo  al desarrollo  principal  objeto de  esta  memoria.  La  elección de  dicha
metodología  resulta relevante  desde el  momento en  el que  las tareas  a realizar  aumentan y  es
necesario mantener una trazabilidad que redunde en beneficio del proyecto.

\begin{figure}[h]
    \begin{center}
      \includegraphics[scale=0.33]{images/metodologia.jpg}
    \end{center}
  \caption[Gestión multiproyecto con GNU-Emacs]{Gestión multiproyecto con GNU-Emacs. Se puede
  observar como desde un mismo entorno de trabajo pueden ser configuradas y gestionadas varias
  tareas de programación, organizacion. etc.}
  \label{fig:metodología}
\end{figure}

  De esta manera, se ha procedido a seleccionar como entorno de trabajo la herramienta GNU Emacs, al
ser ésta una herramienta  que resume e integra todos los requerimientos  anteriores.  De esta manera
ha  podido  seguirse  una  metodología  de  trabajo consistente  en,  por  una  parte,  la  gestión,
documentación y creación  de una agenda del  proyecto (modo Org\footnote{\url{http://orgmode.org}}),
incluyendo la  manipulación de anotaciones,  referencias, enlaces  y ficheros adjuntos  adecuados en
cada etapa.  Por otra parte  se ha facilitado  el desarrollo del  código necesario a  su desarrollo.
Este último  punto se  simplifica en gran  medida al poder  trabajar dentro  de un único  entorno de
desarrollo   con  soporte   avanzado   para  los   lenguajes   necesarios,  incluyendo   capacidades
características  de un  entorno  evolucionado (emacs  code  browser), la  gestión  de navegación  de
fuentes, semánticas (cedet), corrección, compilación, ejecución,  debug, etc..  Por último, el hecho
de poder  configurar el  entorno gráfico visual  (capacidades multi ventana)  a las  necesidades del
proyecto, adaptándolo a  un entorno de desarrollo multi lenguaje  (entorno tabulado) y automatizando
tareas recursivas (lenguaje elisp) ha aportado un grado de ergonomía difícilmente alcanzable de otra
manera. La  segunda parte de esta  memoria permite ver de  manera más en detalle  como funciona esta
estructura de trabajo,  que ha resultado, en  paralelo con el desarrollo  principal, una experiencia
interesante de organización y gestión multiproyecto.
